type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = {
    year: CarYear,
    type: CarType,
    model: CarModel
}

const carYear: CarYear =2001;
const carType: CarType = "Toyota";
const carMode: CarModel = "Corolla";

const car1: Car = {
    year: 2001,
    type: "Nissan",
    model: "XXXX"
}

console.log(car1)